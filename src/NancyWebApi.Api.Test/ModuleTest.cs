using Moq;
using Nancy;
using Nancy.Testing;
using NancyWebApi.Domain.Enums;
using NancyWebApi.Api.Modules.ActionModules;
using NancyWebApi.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Threading.Tasks;
using Xunit;
using NancyWebApi.Domain.Dtos;
using NancyWebApi.Api.Models.ViewModels;

namespace NancyWebApi.Api.Test
{
    public class ModuleTest : ModuleTestBase
    {
        [Fact]
        public async Task ByExistingSection()
        {
            // Given
            var mockArticleService = new Mock<IArticleService>(MockBehavior.Strict);
            mockArticleService.Setup(g => g.GetArticlesAsync(SectionEnum.Arts))
                .ReturnsAsync(new Article[]
                {
                    new Article
                    {
                        Title = "title-1",
                        Url = "url-1",
                        UpdatedDateTime = DateTime.Parse("11/11/1111 1:11:11 PM", CultureInfo.InvariantCulture)
                    },
                    new Article
                    {
                        Title = "another-title",
                        Url = "another-url",
                        UpdatedDateTime = DateTime.Parse("12/22/2222 2:22:22 PM", CultureInfo.InvariantCulture)
                    }
                });

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Module<ArticlesModule>();
                with.Dependency<IArticleService>(mockArticleService.Object);
            });

            var browser = new Browser(bootstrapper);

            // When
            var result = await browser.Get($"/list/arts/first", with =>
            {
                with.HttpsRequest();
            });

            // Then
            var body = JsonConvert.DeserializeObject<ArticleView>(result.Body.AsString());

            Assert.Equal("title-1", body.Heading);
            Assert.Equal("url-1", body.Link);
            Assert.Equal("11/11/1111 1:11:11 PM", body.Updated.ToString());
        }

        [Fact]
        public async Task BySectionArticleDoesNotExist()
        {
            // Given
            var mockArticleService = new Mock<IArticleService>(MockBehavior.Strict);
            mockArticleService.Setup(g => g.GetArticlesAsync(SectionEnum.Arts))
              .ReturnsAsync(new Article[] { });

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Module<ArticlesModule>();
                with.Dependency(mockArticleService.Object);
            });

            var browser = new Browser(bootstrapper);

            // When
            var result = await browser.Get($"/list/arts/first", with =>
            {
                with.HttpsRequest();
            });

            // Then
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public async Task ExistBySectionAndDate()
        {
            // Given
            var testUpdatedDate = DateTime.Parse("11/11/1111 1:11:11 PM", CultureInfo.InvariantCulture);
            var mockArticleService = new Mock<IArticleService>(MockBehavior.Strict);
            mockArticleService.Setup(g => g.GetArticlesBySectionAndDateAsync(SectionEnum.Arts, testUpdatedDate.Date))
              .ReturnsAsync(new Article[] {
                  new Article
                  {
                    Title = "title-1",
                    Url = "url-1",
                    UpdatedDateTime = testUpdatedDate
                  },
                  new Article
                  {
                    Title = "title-1",
                    Url = "url-1",
                    UpdatedDateTime = testUpdatedDate.AddHours(1)
                  }
              });

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Module<ArticlesModule>();
                with.Dependency<IArticleService>(mockArticleService.Object);
            });

            var browser = new Browser(bootstrapper);

            // When
            var result = await browser.Get($"/list/arts/1111-11-11", with =>
            {
                with.HttpsRequest();
            });

            // Then
            var body = JsonConvert.DeserializeObject<ArticleView[]>(result.Body.AsString());

            Assert.Equal("title-1", body[0].Heading);
            Assert.Equal("url-1", body[0].Link);
            Assert.Equal("11/11/1111 1:11:11 PM", body[0].Updated.ToString());
        }

        [Fact]
        public async Task ExistByShortUrl()
        {
            // Given
            var mockArticleService = new Mock<IArticleService>(MockBehavior.Strict);
            mockArticleService.Setup(g => g.GetArticleByShortUrlAsync("XXXXXXX"))
              .ReturnsAsync(new Article
              {
                  Title = "title-1",
                  Url = "url-1",
                  UpdatedDateTime = DateTime.Parse("1/11/1111 1:11:11 PM", CultureInfo.InvariantCulture)
              });

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Module<ArticlesModule>();
                with.Dependency<IArticleService>(mockArticleService.Object);
            });

            var browser = new Browser(bootstrapper);

            // When
            var result = await browser.Get($"/article/XXXXXXX", with =>
            {
                with.HttpsRequest();
            });

            // Then
            var body = JsonConvert.DeserializeObject<ArticleView>(result.Body.AsString());

            Assert.Equal("title-1", body.Heading);
            Assert.Equal("url-1", body.Link);
            Assert.Equal("1/11/1111 1:11:11 PM", body.Updated.ToString());
        }

        [Fact]
        public async Task DoesNotExistByShortUrl()
        {
            // Given
            var mockArticleService = new Mock<IArticleService>(MockBehavior.Strict);
            mockArticleService.Setup(g => g.GetArticleByShortUrlAsync("XXXXXXX"))
              .ReturnsAsync((Article)null);

            var bootstrapper = new DefaultNancyBootstrapper();
            var browser = new Browser(bootstrapper);
            // When
            var result = await browser.Get($"/article/XXXXXXX", with =>
            {
                with.HttpsRequest();
            });

            // Then
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Fact]
        public async Task SectionGroupsByDate()
        {
            // Given
            var testUpdatedDate = DateTime.Parse("11/11/1111", CultureInfo.InvariantCulture);
            var mockArticleService = new Mock<IArticleService>(MockBehavior.Strict);
            mockArticleService.Setup(g => g.GetGroupsByDateAsync(SectionEnum.Arts))
              .ReturnsAsync(new ArticleGroupByDate[] {
                  new ArticleGroupByDate
                  {
                    Total = 10,
                    Date = testUpdatedDate.ToString(validDateFormat)
                  },
                  new ArticleGroupByDate
                  {
                    Total = 11,
                    Date = testUpdatedDate.AddDays(1).ToString(validDateFormat)
                  }
              });

            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Module<ArticlesModule>();
                with.Dependency<IArticleService>(mockArticleService.Object);
            });

            var browser = new Browser(bootstrapper);

            // When
            var result = await browser.Get($"/group/arts", with =>
            {
                with.HttpsRequest();
            });

            // Then
            var body = JsonConvert.DeserializeObject<ArticleGroupByDate[]>(result.Body.AsString());

            Assert.Equal("1111-11-11", body[0].Date);
            Assert.Equal(10.ToString(), body[0].Total.ToString());
            Assert.Equal("1111-11-12", body[1].Date);
            Assert.Equal(11.ToString(), body[1].Total.ToString());
        }

        [Theory]
        [InlineData("/")]
        [InlineData("/list/arts")]
        [InlineData("/list/arts/first")]
        [InlineData("/list/arts/1111-11-11")]
        [InlineData("/article/XXXXXXX")]
        [InlineData("/group/arts")]
        public async Task OkWhenRouteExists(string route)
        {
            var configurableBootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Module<ArticlesModule>();
                with.Dependency(_mockArticleService.Object);
            });

            var browser = new Browser(configurableBootstrapper);

            // When
            var result = await browser.Get(route, with =>
            {
                with.HttpsRequest();
            });

            // Then
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public async Task WhenRouteDoesNotExist()
        {
            // Given
            var mockArticleService = new Mock<IArticleService>();
            mockArticleService.Setup(g => g.GetArticlesAsync(SectionEnum.Arts))
              .ReturnsAsync(new Article[] { });

            var configurableBootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Module<ArticlesModule>();
                with.Dependency<IArticleService>(mockArticleService.Object);
            });

            var browser = new Browser(configurableBootstrapper);

            // When
            var result = await browser.Get("/unexisting-route", with =>
            {
                with.HttpsRequest();
            });

            // Then
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }
    }
}
