﻿using NancyWebApi.Domain.Dtos;
using NancyWebApi.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NancyWebApi.Services.Interfaces
{
    public interface IArticleService
    {
        // Get articles by section.
        Task<IEnumerable<Article>> GetArticlesAsync(SectionEnum section);

        // Get article by short url.
        Task<Article> GetArticleByShortUrlAsync(string shortUrl);

        // Get articles filtered by section and updated date.
        Task<IEnumerable<Article>> GetArticlesBySectionAndDateAsync(SectionEnum section, DateTime updatedDateTime);

        // Get Articles groups filtered by section and group by updated dates. 
        Task<IEnumerable<ArticleGroupByDate>> GetGroupsByDateAsync(SectionEnum section);
    }
}
