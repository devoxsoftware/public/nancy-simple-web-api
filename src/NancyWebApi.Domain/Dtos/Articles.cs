﻿using System.Collections.Generic;

namespace NancyWebApi.Domain.Dtos
{
    public class Articles
    {
        public IEnumerable<Article> Results { get; set; }
    }
}
