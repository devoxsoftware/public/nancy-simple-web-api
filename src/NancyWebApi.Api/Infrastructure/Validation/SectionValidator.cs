﻿using FluentValidation;
using NancyWebApi.Domain.Enums;
using NancyWebApi.Api.Infrastructure.Validators.Extensions;
using NancyWebApi.Api.Models.QueryParamModels;

namespace NancyWebApi.Api.Infrastructure.Validation
{
    public class SectionValidator : AbstractValidator<SectionQueryParams>
    {
        public SectionValidator()
        {
            RuleFor(x => x.Section).IsInEnum(typeof(SectionEnum));
        }
    }
}
