﻿using Nancy;
using Nancy.ModelBinding;
using Nancy.Validation;
using NancyWebApi.Domain.Enums;
using NancyWebApi.Services.Interfaces;
using System.Linq;
using System;
using System.Globalization;
using Nancy.Configuration;
using NancyWebApi.Api.Models.ViewModels;
using NancyWebApi.Api.Models.QueryParamModels;
using Mapster;
using System.Collections.Generic;

namespace NancyWebApi.Api.Modules.ActionModules
{
    public class ArticlesModule : NancyModuleBase
    {
        private readonly IArticleService _articleService;

        private const string _validDateFormat = "yyyy-MM-dd";

        public ArticlesModule(IArticleService articleService, INancyEnvironment environment)
        {
            _articleService = articleService;

            Get("/", _ =>
            {
                return "NancyWebApi works!";
            });

            Get("/list/{section}", async _ =>
            {
                var @params = this.Bind<SectionQueryParams>();
                var validationResult = this.Validate(@params);

                if (!validationResult.IsValid)
                {
                    return Error(this, validationResult);
                }

                var result = await _articleService.GetArticlesAsync(ParseSection(@params.Section));

                return Response.AsJson(result?.Adapt<IEnumerable<ArticleView>>());
            });

            Get("/list/{section}/first", async _ =>
            {
                var @params = this.Bind<SectionQueryParams>();
                var validationResult = this.Validate(@params);

                if (!validationResult.IsValid)
                {
                    return Error(this, validationResult);
                }

                var results = await _articleService.GetArticlesAsync(
                    ParseSection(@params.Section)
                );
                var result = results.FirstOrDefault();

                if (result is null)
                {
                    return NotFound(this);
                }

                return Response.AsJson(result?.Adapt<ArticleView>());
            });

            Get("/list/{section}/{updatedDate}", async _ =>
            {
                var @params = this.Bind<SectionAndDateQueryParams>();
                var validationResult = this.Validate(@params);

                if (!validationResult.IsValid)
                {
                    return Error(this, validationResult);
                }

                var result = await _articleService.GetArticlesBySectionAndDateAsync(
                    ParseSection(@params.Section),
                    DateTime.ParseExact(@params.UpdatedDate, _validDateFormat, CultureInfo.InvariantCulture)
                );

                return Response.AsJson(result?.Adapt<IEnumerable<ArticleView>>());
            });

            Get("/article/{shortUrl}", async _ =>
            {
                var @params = this.Bind<ShortUrlQueryParams>();
                var validationResult = this.Validate(@params);

                if (!validationResult.IsValid)
                {
                    return Error(this, validationResult);
                }

                var result = await _articleService.GetArticleByShortUrlAsync(
                    @params.ShortUrl
                );

                if (result is null)
                {
                    return NotFound(this);
                }

                return Response.AsJson(result?.Adapt<ArticleView>());
            });

            Get("/group/{section}", async _ =>
            {
                var @params = this.Bind<SectionQueryParams>();
                var validationResult = this.Validate(@params);

                if (!validationResult.IsValid)
                {
                    return Error(this, validationResult);
                }

                var result = await _articleService.GetGroupsByDateAsync(ParseSection(@params.Section));
                return Response.AsJson(result?.Adapt<IEnumerable<ArticleGroupByDateView>>());
            });
        }

        private SectionEnum ParseSection(string section)
        {
            return (SectionEnum) Enum.Parse(typeof(SectionEnum), section, true);
        }
    }
}
