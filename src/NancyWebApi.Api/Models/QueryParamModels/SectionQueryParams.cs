﻿namespace NancyWebApi.Api.Models.QueryParamModels
{
    public class SectionQueryParams
    {
        public string Section { get; set; }
    }
}
