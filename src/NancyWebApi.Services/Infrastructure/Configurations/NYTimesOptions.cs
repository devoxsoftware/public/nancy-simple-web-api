﻿namespace NancyWebApi.Services.Infrastructure.Configurations
{
    public class NYTimesOptions
    {
        public static string SectionName { get; set; } = "NYTimesApi";
        public string BaseUrl { get; set; }
        public string TopStoriesPath { get; set; }
        public string ShortUrl { get; set; }
        public string ApiKey { get; set; }
    }
}
