﻿using Microsoft.Extensions.Options;
using NancyWebApi.Domain.Dtos;
using NancyWebApi.Domain.Enums;
using NancyWebApi.Services.Infrastructure.Configurations;
using NancyWebApi.Services.Interfaces;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NancyWebApi.Services
{
    public class ArticleService : IArticleService
    {
        private readonly IRestClient _restClient;
        private readonly NYTimesOptions _nyTimesApiConfig;

        private const string _validDateFormat = "yyyy-MM-dd";


        public ArticleService(IRestClient restClient, IOptions<NYTimesOptions> options)
        {
            _restClient = restClient;
            _nyTimesApiConfig = options.Value;
        }

        public async Task<IEnumerable<Article>> GetArticlesAsync(SectionEnum section)
        {
            return await GetArticlesBySectionAsync(section);
        }

        public async Task<Article> GetArticleByShortUrlAsync(string shortUrl)
        {
            var articles = await GetArticlesBySectionAsync(SectionEnum.Home);
            return articles.SingleOrDefault(x => x.ShortUrl == _nyTimesApiConfig.ShortUrl + shortUrl);
        }

        public async Task<IEnumerable<Article>> GetArticlesBySectionAndDateAsync(SectionEnum section, DateTime updatedDate)
        {
            var articles = await GetArticlesBySectionAsync(section);
            return articles.Where(x => x.UpdatedDateTime.Date == updatedDate.Date);
        }

        public async Task<IEnumerable<ArticleGroupByDate>> GetGroupsByDateAsync(SectionEnum section)
        {
            var articles = await GetArticlesBySectionAsync(section);
            return articles
              .GroupBy(x => x.UpdatedDateTime.Date)
              .Select(x => new ArticleGroupByDate
              {
                  Total = x.Count(),
                  Date = x.Key.ToString(_validDateFormat)
              });
        }

        private async Task<IEnumerable<Article>> GetArticlesBySectionAsync(SectionEnum section)
        {
            _restClient.BaseUrl = new Uri(_nyTimesApiConfig.BaseUrl);

            var request = new RestRequest(_nyTimesApiConfig.TopStoriesPath, Method.GET);

            request.AddUrlSegment("section", section.ToString().ToLower());
            request.AddParameter("api-key", _nyTimesApiConfig.ApiKey);

            var restResponse = await _restClient.ExecuteTaskAsync(request);

            
            if (!restResponse.IsSuccessful)
                throw new Exception(restResponse.ErrorMessage);

            return JsonConvert.DeserializeObject<Articles>(restResponse.Content).Results;
        }
    }
}
