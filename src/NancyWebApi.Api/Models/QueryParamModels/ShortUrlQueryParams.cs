﻿namespace NancyWebApi.Api.Models.QueryParamModels
{
    public class ShortUrlQueryParams
    {
        public string ShortUrl { get; set; }
    }
}
