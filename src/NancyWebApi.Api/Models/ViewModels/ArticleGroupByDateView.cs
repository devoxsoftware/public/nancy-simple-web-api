﻿namespace NancyWebApi.Api.Models.ViewModels
{
    public class ArticleGroupByDateView
    {
        public string Date { get; set; }
        public int Total { get; set; }
    }
}
