using Microsoft.Extensions.Options;
using Moq;
using NancyWebApi.Services.Infrastructure.Configurations;
using RestSharp;

namespace NancyWebApi.Services.Test
{
    public class ArticleServiceTestBase
    {
        protected Mock<IOptions<NYTimesOptions>> _mockNYTimesApiConfig = new Mock<IOptions<NYTimesOptions>>();

        protected Mock<IRestClient> _mockRestClient = new Mock<IRestClient>();
        protected Mock<IRestResponse> _mockRestResponse = new Mock<IRestResponse>();

        public ArticleServiceTestBase()
        {
            _mockNYTimesApiConfig.SetupGet(c => c.Value).Returns(new NYTimesOptions
                {
                    BaseUrl = "http://localhost:5000/",
                    TopStoriesPath = "top/stories/path",
                    ShortUrl = "https://nyti.ms/",
                    ApiKey = "test-api-key"
                }
            );

            _mockRestClient.Setup(c => c.ExecuteTaskAsync(It.IsAny<IRestRequest>())).ReturnsAsync(_mockRestResponse.Object);
            _mockRestResponse.SetupGet(r => r.IsSuccessful).Returns(true);
            _mockRestResponse.SetupGet(r => r.Content).Returns("{results:[]}");
        }
    }
}

