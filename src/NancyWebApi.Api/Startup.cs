﻿using Mapster;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nancy.Owin;
using NancyWebApi.Api.Modules;
using NancyWebApi.Api.Models.ViewModels;
using NancyWebApi.Services;
using NancyWebApi.Services.Infrastructure.Configurations;
using NancyWebApi.Services.Interfaces;
using NancyWebApi.Domain.Dtos;
using RestSharp;

namespace NancyWebApi.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IServiceCollection Services { get; private set; }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IRestClient, RestClient>();
            services.AddScoped<IArticleService, ArticleService>();
            services.Configure<NYTimesOptions>(Configuration.GetSection(NYTimesOptions.SectionName));

            Services = services;
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            MapsterSetup();

            app.UseOwin().UseNancy(x => x.Bootstrapper = new NancyBootstraper(Services));
        }

        public void MapsterSetup()
        {
            TypeAdapterConfig<Article, ArticleView>.NewConfig()
                            .Map(dest => dest.Heading, src => src.Title)
                            .Map(dest => dest.Link, src => src.Url)
                            .Map(dest => dest.Updated, src => src.UpdatedDateTime);
        }
    }
}
