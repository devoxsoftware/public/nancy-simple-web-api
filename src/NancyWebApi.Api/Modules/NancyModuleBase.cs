﻿using Nancy;
using Nancy.Validation;
using NancyWebApi.Api.Modules.ActionModules;

namespace NancyWebApi.Api.Modules
{
    public class NancyModuleBase : NancyModule
    {
        protected Response NotFound(ArticlesModule articlesModule)
            => articlesModule.Response.AsJson(new { status = nameof(HttpStatusCode.NotFound) }, HttpStatusCode.NotFound);

        protected Response Error(ArticlesModule articlesModule, ModelValidationResult modelValidationResult)
            => articlesModule.Response.AsJson(modelValidationResult, HttpStatusCode.BadRequest);
    }
}
