﻿namespace NancyWebApi.Api.Models.QueryParamModels
{
    public class SectionAndDateQueryParams
    {
        public string Section { get; set; }
        public string UpdatedDate { get; set; }
    }
}
