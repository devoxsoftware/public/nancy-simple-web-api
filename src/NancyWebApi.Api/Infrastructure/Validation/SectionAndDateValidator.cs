﻿using FluentValidation;
using NancyWebApi.Domain.Enums;
using NancyWebApi.Api.Infrastructure.Validators.Extensions;
using NancyWebApi.Api.Models.QueryParamModels;

namespace NancyWebApi.Api.Infrastructure.Validators
{
    public class SectionAndDateValidator : AbstractValidator<SectionAndDateQueryParams>
    {
        public SectionAndDateValidator()
        {
            RuleFor(x => x.UpdatedDate).MustBeValidDate("yyyy-MM-dd");
            RuleFor(x => x.Section).IsInEnum(typeof(SectionEnum));
        }
    }
}
