using Mapster;
using Moq;
using NancyWebApi.Api.Models.ViewModels;
using NancyWebApi.Domain.Dtos;
using NancyWebApi.Domain.Enums;
using NancyWebApi.Services.Interfaces;

namespace NancyWebApi.Api.Test
{
    public class ModuleTestBase
    {
        protected Mock<IArticleService> _mockArticleService = new Mock<IArticleService>(MockBehavior.Loose);

        public const string validDateFormat = "yyyy-MM-dd";

        public ModuleTestBase()
        {            
            _mockArticleService.Setup(s => s.GetArticleByShortUrlAsync(It.IsAny<string>()))
              .ReturnsAsync(new Article { });
            _mockArticleService.Setup(s => s.GetArticlesAsync(It.IsAny<SectionEnum>()))
              .ReturnsAsync(new Article[] { new Article() });

            SetupMapster();
        }

        public void SetupMapster()
        {
            TypeAdapterConfig<Article, ArticleView>.NewConfig()
                .Map(dest => dest.Heading, src => src.Title)
                .Map(dest => dest.Link, src => src.Url)
                .Map(dest => dest.Updated, src => src.UpdatedDateTime);
        }

    }
}
