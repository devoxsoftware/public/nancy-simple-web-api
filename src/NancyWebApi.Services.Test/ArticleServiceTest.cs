using Moq;
using NancyWebApi.Domain.Enums;
using RestSharp;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NancyWebApi.Services.Test
{
    public class ArticleServiceTest : ArticleServiceTestBase
    {
        public class GetArticlesBySection : ArticleServiceTestBase
        {
            [Fact]
            public async Task UsesValidRequestUrl()
            {
                // Act
                var service = new ArticleService(_mockRestClient.Object, _mockNYTimesApiConfig.Object);
                await service.GetArticlesAsync(SectionEnum.Arts);

                // Assert
                _mockRestClient.VerifySet(f => f.BaseUrl = It.Is<Uri>(u => u.ToString() == "http://localhost:5000/"), Times.Once);
                _mockRestClient.Verify(c => c.ExecuteTaskAsync(It.Is<RestRequest>(r =>
                  r.Resource == _mockNYTimesApiConfig.Object.Value.TopStoriesPath &&
                  r.Parameters.Any(p =>
                    p.Name == "api-key" && p.Value.ToString() == "test-api-key" ||
                    p.Name == "section" && p.Value.ToString() == "arts"
                  ))), Times.Once);
            }
        }

        public class GetArticleByShortUrl : ArticleServiceTestBase
        {
            [Fact]
            public async Task UsesValidRequestUrl()
            {
                // Act
                var service = new ArticleService(_mockRestClient.Object, _mockNYTimesApiConfig.Object);
                await service.GetArticlesAsync(SectionEnum.Arts);

                // Assert
                _mockRestClient.VerifySet(f => f.BaseUrl = It.Is<Uri>(u => u.ToString() == "http://localhost:5000/"), Times.Once);
                _mockRestClient.Verify(c => c.ExecuteTaskAsync(It.Is<RestRequest>(r =>
                  r.Resource == _mockNYTimesApiConfig.Object.Value.TopStoriesPath &&
                  r.Parameters.Any(p =>
                    p.Name == "api-key" && p.Value.ToString() == "test-api-key" ||
                    p.Name == "section" && p.Value.ToString() == "arts"
                  ))), Times.Once);
            }

            [Fact]
            public async Task TakesSingleFoundArticle()
            {
                // Arrange
                _mockRestResponse.SetupGet(r => r.Content)
                  .Returns(@"
                    {
                      results: [
                        {
                          title: 'title-1',
                          url: 'url-1',
                          updated_date: '1/11/1111 1:11:11 PM',
                          short_url: 'https://nyti.ms/XXXXXXX'
                        }
                      ]
                    }
                  ");

                // Act
                var service = new ArticleService(_mockRestClient.Object, _mockNYTimesApiConfig.Object);
                var result = await service.GetArticleByShortUrlAsync("XXXXXXX");

                // Assert
                Assert.Equal("title-1", result.Title);
            }

            [Fact]
            public async Task ThrowsExceptionIfArticleNotSingle()
            {
                // Arrange
                _mockRestResponse.SetupGet(r => r.Content)
                  .Returns(@"
                    {
                      results: [
                        {
                          title: 'title-1',
                          url: 'url-1',
                          updated_date: '1/11/1111 1:11:11 PM',
                          short_url: 'https://nyti.ms/XXXXXXX'
                        },
                        {
                          title: 'title-1',
                          url: 'url-1',
                          updated_date: '1/11/1111 1:11:11 PM',
                          short_url: 'https://nyti.ms/XXXXXXX'
                        }
                      ]
                    }
                  ");

                // Act
                var service = new ArticleService(_mockRestClient.Object, _mockNYTimesApiConfig.Object);
                await Assert.ThrowsAsync<InvalidOperationException>(() => service.GetArticleByShortUrlAsync("XXXXXXX"));
            }

            [Fact]
            public async Task ReturnsNullForNotFoundArticle()
            {
                // Arrange
                _mockRestResponse.SetupGet(r => r.Content)
                  .Returns(@"
                    {
                      results: [
                        {
                          title: 'title-2',
                          url: 'url-2',
                          updated_date: '2/22/2222 2:22:22 PM',
                          short_url: 'https://nyti.ms/ZZZZZZZ'
                        }
                      ]
                    }
                  ");

                // Act
                var service = new ArticleService(_mockRestClient.Object, _mockNYTimesApiConfig.Object);
                var result = await service.GetArticleByShortUrlAsync("XXXXXXX");

                // Assert
                Assert.Null(result);
            }
        }

        public class GetArticlesBySectionAndDate : ArticleServiceTestBase
        {
            [Fact]
            public async Task UsesValidRequestUrl()
            {
                // Act
                var service = new ArticleService(_mockRestClient.Object, _mockNYTimesApiConfig.Object);
                await service.GetArticlesBySectionAndDateAsync(SectionEnum.Arts, DateTime.Now);

                // Assert
                _mockRestClient.VerifySet(f => f.BaseUrl = It.Is<Uri>(u => u.ToString() == "http://localhost:5000/"), Times.Once);
                _mockRestClient.Verify(c => c.ExecuteTaskAsync(It.Is<RestRequest>(r =>
                  r.Parameters.Any(p =>
                    p.Name == "api-key" && p.Value.ToString() == "test-api-key" ||
                    p.Name == "section" && p.Value.ToString() == "arts"
                  ))), Times.Once);
            }

            [Fact]
            public async Task FiltersArticlesByUpdatedDate()
            {
                // Arrange
                _mockRestResponse.SetupGet(r => r.Content)
                  .Returns(@"
                    {
                      results: [
                        {
                          title: 'title-1',
                          updated_date: '1/11/1111 1:11:11 PM',
                          url: 'url-1'
                        },
                        {
                          title: 'title-2',
                          updated_date: '2/22/2222 2:22:22 PM',
                          url: 'url-2'
                        }
                      ]
                    }
                  ");

                // Act
                var service = new ArticleService(_mockRestClient.Object, _mockNYTimesApiConfig.Object);
                var result = await service.GetArticlesBySectionAndDateAsync(SectionEnum.Arts, DateTime.Parse("1/11/1111"));

                // Assert
                Assert.Equal("title-1", result.Single().Title);
            }
        }


        public class GetArticleGroupsBySection : ArticleServiceTestBase
        {
            [Fact]
            public async Task UsesValidRequestUrl()
            {
                // Act
                var service = new ArticleService(_mockRestClient.Object, _mockNYTimesApiConfig.Object);
                await service.GetGroupsByDateAsync(SectionEnum.Arts);

                // Assert
                _mockRestClient.VerifySet(f => f.BaseUrl = It.Is<Uri>(u => u.ToString() == "http://localhost:5000/"), Times.Once);
                _mockRestClient.Verify(c => c.ExecuteTaskAsync(It.Is<RestRequest>(r =>
                  r.Resource == _mockNYTimesApiConfig.Object.Value.TopStoriesPath &&
                  r.Parameters.Any(p =>
                    p.Name == "api-key" && p.Value.ToString() == "test-api-key" ||
                    p.Name == "section" && p.Value.ToString() == "arts"
                  ))), Times.Once);
            }

            [Fact]
            public async Task GroupsByDate()
            {
                // Arrange
                _mockRestResponse.SetupGet(r => r.Content)
                  .Returns(@"
                    {
                      results: [
                        {
                          updated_date: '1/11/1111 1:11:11 PM'
                        },
                        {
                          updated_date: '1/11/1111 1:11:11 PM'
                        },
                        {
                          updated_date: '2/22/2222 2:22:22 PM'
                        }
                      ]
                    }
                  ");

                // Act
                var service = new ArticleService(_mockRestClient.Object, _mockNYTimesApiConfig.Object);
                var result = await service.GetGroupsByDateAsync(SectionEnum.Arts);

                // Assert
                Assert.Equal(2, result.Count());

                var group = result.ElementAt(0);
                Assert.Equal(2, group.Total);

                group = result.ElementAt(1);
                Assert.Equal(1, group.Total);
            }
        }

        [Fact]
        public async Task ThrowsExceptionIfResponseNotSuccessful()
        {
            // Arrange
            _mockRestResponse.SetupGet(r => r.IsSuccessful).Returns(false);
            _mockRestResponse.SetupGet(r => r.ErrorMessage).Returns("test-error-message");
            _mockRestResponse.SetupGet(r => r.Content)
              .Returns(@"
                    {
                      results: []
                    }
                  ");

            // Act
            var service = new ArticleService(_mockRestClient.Object, _mockNYTimesApiConfig.Object);

            async Task assert(Func<Task> act)
            {
                var exception = await Assert.ThrowsAsync<Exception>(act);
                Assert.Equal("test-error-message", exception.Message);
            }

            await assert(() => service.GetArticleByShortUrlAsync("XXXXXXX"));
            await assert(() => service.GetArticlesAsync(SectionEnum.Arts));
            await assert(() => service.GetArticlesBySectionAndDateAsync(SectionEnum.Arts, DateTime.Now));
            await assert(() => service.GetGroupsByDateAsync(SectionEnum.Arts));
        }
    }
}

