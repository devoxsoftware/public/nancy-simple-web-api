﻿using FluentValidation;
using NancyWebApi.Api.Models.QueryParamModels;

namespace NancyWebApi.Api.Infrastructure.Validation
{
    public class ShortUrlValidator : AbstractValidator<ShortUrlQueryParams>
    {
        public ShortUrlValidator()
        {
            RuleFor(x => x.ShortUrl).Length(7).WithMessage("Short url should have XXXXXXX format.");
        }
    }
}
