﻿namespace NancyWebApi.Domain.Dtos
{
    public class ArticleGroupByDate
    {
        public string Date { get; set; }
        public int Total { get; set; }
    }
}
